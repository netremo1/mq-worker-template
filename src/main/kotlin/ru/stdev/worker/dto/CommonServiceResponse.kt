package ru.stdev.worker.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class CommonServiceResponse(
    @JsonProperty("success")
    val success: Boolean,
    @JsonProperty("error")
    val error: String? = null,
    @JsonProperty("result")
    val result: Any? = null
)