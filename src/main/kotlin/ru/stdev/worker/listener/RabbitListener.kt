package ru.stdev.worker.listener

import com.beust.klaxon.Klaxon
import org.slf4j.LoggerFactory
import org.springframework.amqp.core.Message
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component
import ru.stdev.worker.common.Command

@Component
class RabbitListener() {
    private val logger = LoggerFactory.getLogger("RECEIVER")

    @ExperimentalStdlibApi
    @RabbitListener(queues = ["#{workerQueue.name}"], concurrency = "10")
    fun receive(rabbitMessage: Message) {
        logger.info("Receiver message: $rabbitMessage")

        try {

            val command = Klaxon().parse<Command>(rabbitMessage.body.decodeToString())
            command!!.doWork()

        } catch (e: Exception) {
            println(e)
        }
    }

}